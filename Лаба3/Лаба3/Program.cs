﻿using System;

namespace Лаба3

{

    public static class String
    {
        public static string PartString(this string text, int num)
        {
            string ans = null;
            for (int i = num - 1; i < text.Length; i = i + num)
                ans = ans + text[i];
            return ans;
        }
    }

    public static class Int
    {
        public static int Factorial(this int num)
        {
            int a = 1;
            for (int i = 2; i <= num; i++)
                a = a * i;
            return a;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            string text = Console.ReadLine();
            text = text.PartString(2);
            int num = int.Parse(Console.ReadLine());
            num = num.Factorial();
            Console.WriteLine("{0},   {1}", text, num);
        }
    }
}
